---
title: "Italy Movies"
author: "Chen Arazi Haroush & Ariel Vinograd"
date: "14.09.2019"
framework   : io2012        
highlighter : highlight.js  
hitheme     : tomorrow   
output: 
    html_document:
      theme: flatly
  
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

##Introduction
This dataset provides data regarding the rates and the number of votes assigned to each film by Italian users, which can be compared with data available on other movie-related websites.
The data was taken from Kaggle website: "FilmTV movies dataset" https://www.kaggle.com/stefanoleone992/filmtv-movies-dataset/version/2
Data has been scraped from the publicly available website https://www.filmtv.it.

###**Our research question**
Given details of a particular movie, we want to predict it's success.
Details as genre, season release, and duration of it.
This will be important for film industry in Italy. They will be able to better understand what type of movies will be worth producing and when.

##Data
The raw data contains 46,907 movies (rows) and 10 attributes (columns):
Each row represents a movie available on 'FilmTV.it', with the original title, year, genre, duration, country, director, actors, average vote and votes. 'average vote' is the score of the movie from 1 to 10, and 'votes' is the number of people that voted for that film, while we refer to it as the number of viewers ()

###Loading packages
```{r, cache=FALSE, echo=TRUE, message=FALSE, warning=FALSE, tidy=FALSE}
#library we used in this project
library(xml2)
library(rvest)
library(stringr)
library(cellranger)
library(ggplot2)
library(plyr)
library(dplyr)
library(MASS)
library(lmtest)
library(knitr)
```

###Presenting the original dataset
```{r}
italymovies<-read.csv("C:/Users/ariel vinograd/Desktop/P.P/FilmTV Dataset - ENG.csv")
kable(head(italymovies))
```

###Cleaning and filtering the data
We pulled the data from a public Internet repository, filtered it according to the most informative genres, for example, the amount of films, and also filtered movies that were not sufficiently indicative and reliable: 
<br/> -less than 30 votes is not indicative in our opinion
<br/> -we define a movie as a film that last at least 60 minutes, and maximum 180 minutes.
```{r}
italymoviesnew<-filter(italymovies, votes >= 30)

italymoviesnew <- filter(italymoviesnew, duration<180);italymoviesnew<-filter(italymoviesnew, duration>60)

#taking the genres with highest number of films. we chose six so we will have informtive data and still enough data to analys
head(as.data.frame(rev(sort(table(italymoviesnew$genre)))), n = 10)
spesific.genre <- c("Action","Comedy","Drama","Horror","Fantasy","Thriller")
IDselectedmovies <- italymoviesnew$filmtv_ID[italymoviesnew$genre%in%spesific.genre]
italymoviesnew <- subset(italymoviesnew, subset = italymoviesnew$filmtv_ID %in% IDselectedmovies)

kable(head(italymoviesnew))
```

###Saving rows with valid films names
```{r}
#create a vector of the ABC, numbers and punctuation
num<-1:26
ABC<-num_to_letter(num)
abc<-tolower(ABC)
legitList<-c(ABC,abc,0:9," ", "-", ",", "_", ".", "!", "'", ":", "|")

goodRows <- c(0,0,0)

#Check if the movie name contains invalid characters
for(i in 1:nrow(italymoviesnew)){
  title <- strsplit(as.character(italymoviesnew$film_title[i]), split = "")
  title <- as.vector(title[[1]])
  delete_index <- all(title%in%legitList) 
  if(delete_index)
    goodRows[i] <- c(i)
  else
    goodRows[i] <- c(NA)
}
goodRows <- goodRows[which(!is.na(goodRows))] #Checking and saving the lines that have a valid name
italymoviesnew <- italymoviesnew[goodRows,] 
```

###Create a column that catalogue the duration of the movies 
```{r}
for (j in 1:nrow(italymoviesnew)) {
  italymoviesnew$popularity[j]<-italymoviesnew$avg_vote[j]*italymoviesnew$votes[j]
}

for (j in 1:nrow(italymoviesnew)) {
  if (0 <= italymoviesnew$duration[j] & italymoviesnew$duration[j] < 90)
        italymoviesnew$length[j]<-1 #"short"
  else if(90 <= italymoviesnew$duration[j] & italymoviesnew$duration[j] < 120)
        italymoviesnew$length[j]<-2 #"normal"
  else if(120 <= italymoviesnew$duration[j] & italymoviesnew$duration[j] < 180)
        italymoviesnew$length[j]<-3 #"long"
}
```

##Additional data, **self-collected** from the IMDB website: https://www.imdb.com
###We created a function that collects from the Internet the name of the month which the movie came out at, and checks if it is a valid month's name
```{r}
MoviePage <- function(movie)
{
  template = "https://www.imdb.com/find?q=%s&s=tt" #The link of the site where we will perform the search (the movie name goes to %s), with the search bar on the page
  name1 = gsub(" ","+",movie) #Taking the name of the movie and placing "+" instead of space between words
  url = sprintf(template,name1) #insde template it insert the name1 of the movie we want instead of %s. it gives us the specific link for the webpage relevant for name1.
  websearch = httr::GET(url) #apply web search of the url. we get list of movies with that specific name, but different year for example.
  WebPage <- read_html(websearch) #like F12, it gives us the HTML of that webpage
  
  # test if the movie isn't exist in the iMDB databank
  WebPage <- html_nodes(WebPage, 'h1.findHeader') 
  Header <- html_text(WebPage)
  Substring2 <- substr(Header, start = 1, stop = 20)
  noResult<-c("No results found for")
  if(noResult==Substring2) #If the search result is "No results found", the condition is met
  {
    return("LOL") #return a temporary month name
  }
  else
  {
    WebPage <- read_html(websearch) #we want the most relevant movie from that list.
    title_html <- html_nodes(WebPage, 'td')
    Substring <- substr(title_html[2], start = 35, stop = 67) #takes the movie ID
    url1 = paste0("https://www.imdb.com", Substring) #search again, but this time search by movie ID
    WebSearch1 <- httr::GET(url1) #apply the search
    WebPage1 <- read_html(WebSearch1) #read the HTML of the result
  
    #scrape season of the product
    Season_Realese <- html_nodes(WebPage1, 'div.subtext')
    Season_Realese <- html_text(Season_Realese)
    #remove spaces and new line
    Season_Realese <- str_replace_all(Season_Realese, "[\r \n \t]" , "")
    #replace the "|" with "," for R could recognized
    Season_Realese <- str_replace_all(Season_Realese, "[|]" , ",")
    #now we can separate phrase by the "," we insert
    Season_Realese <- strsplit(Season_Realese, split = ",")
    #we reversed the result (because the month is the last element in the line) and take the first element
    Season_Realese <- rev(Season_Realese[[1]])[1]
    #split the phrase to characters
    Season_Realese <- strsplit(Season_Realese, split = "|")
    Season_Realese <- Season_Realese[[1]]
    #From the list of characters we want T where there are letters
    k <- suppressWarnings(!as.integer(Season_Realese))
    #We got NA where there are numbers and we want to keep the locations of characters that do not have NA but have letters
    k <- which(!is.na(k))
    #If we have NA already its first character then there is no month name and we will return LOL
#This condition solves exceptional cases where there is no one or two digit structure and then a month's name
    if(is.na(k[1]))
      return("LOL")
    else
    {
      #create two variable, with them we sign where the month is begin and end
      kstart <- 0
      kend <- 0
      if(length(k[k==2]) || length(k[k==1])) #for cases that the number of the day is two or one digits
      {
        if(length(k[k==2]))
        {
          kstart<-k[-(1:2)]
          kend<-min(kstart)
          mth <- c(3:(kend-1))
        }else
        {
          kstart<-k[-1]
          kend<-min(kstart)
          mth <- c(2:(kend-1))
        }
      }else
      {
        kstart <- k
        kend<-min(kstart)
        mth <- c(1:(kend-1))
      }
      return(paste(Season_Realese[mth], collapse = ""))
    }
  }
}
```

###Creating a season column in which the movie was published
```{r}
#italymoviesnew$Release_Season <- NA

#x <- 1:nrow(italymoviesnew)
#for (j in x){
#  movie <- italymoviesnew$film_title[j]
#  italymoviesnew$Release_Season[j] <- MoviePage(movie)
#}

#write.csv(italymoviesnew,"C:/Users/ariel vinograd/Desktop/P.P/italymoviesnew.csv")
```

###Final filtering - taking only the relevant columns
```{r}
italymoviesnew <- read.csv("C:/Users/ariel vinograd/Desktop/P.P/NEWFILE.csv")

#Delete unnecessary columns
italymoviesnew <- italymoviesnew[,-c(1:5, 11:12)]

#Delete all rows that did not contain a valid month name
italymoviesnew <- filter(italymoviesnew, italymoviesnew$Release_Season%in%month.name)
italymoviesnew <- as.data.frame(italymoviesnew)
italymoviesnew$Release_Season <- as.vector(italymoviesnew$Release_Season)


# Update the column of the months by the season the movie came out
for (j in 1:nrow(italymoviesnew))
{
  if(italymoviesnew$Release_Season[j]%in%month.name[3:5])
        italymoviesnew$Release_Season[j] <- 'Spring'
  
  else if(italymoviesnew$Release_Season[j]%in%month.name[6:8])
        italymoviesnew$Release_Season[j] <- "Summer"
  
  else if(italymoviesnew$Release_Season[j]%in%month.name[9:11])
        italymoviesnew$Release_Season[j] <- "Autumn"
  
  else if(italymoviesnew$Release_Season[j]%in%month.name[c(12,1,2)])
        italymoviesnew$Release_Season[j] <- "Winter"
}

write.csv(italymoviesnew,"C:/Users/ariel vinograd/Desktop/P.P/italymoviesnew.csv")
```

##Exploratory data analysis
###Popularity of a genre
Visualization of the releation between amount of viewers and amount of films in each genre 
```{r,echo=FALSE}
total.films<-1:6
total.viewers<-1:6
for (i in 1:6 )
{
  total.films[i]<-length(which(italymoviesnew$genre==spesific.genre[i]))
  total.viewers[i]<-sum(italymoviesnew$votes[italymoviesnew$genre==spesific.genre[i]])
}
names(total.viewers) <- spesific.genre
names(total.films) <- spesific.genre

Table <- as.data.frame(cbind(total.films, total.viewers))
Genre <- row.names(Table)
qplot(x = Table$total.films, y = Table$total.viewers, col=Genre, xlab = "Amount of films" ,ylab = "Amount of viewers", size=3)
```
It make sense that the more movies we have in the genre- more people will watch this genre. We cannot tell the direction of the causation, bat it looks like there is a conection.
For now, we can calculate the ratio `total.viewers/total.films` and we get:

```{r,echo=FALSE}
attractiveGenre<-total.viewers/total.films
round(attractiveGenre, 1)
```
Fantasy is the most attractive genre! <br/> while comedy seems to be the less attrractive.

###Divide the information into two groups for training and test results
we apply `set.seed` in order to get outcomes not dependent on the randomalization process of dividing the data.
```{r}
set.seed(2)
#Taking 80% of the Delta for training
TrainData <- italymoviesnew[sample(nrow(italymoviesnew), nrow(italymoviesnew)*0.8), ]
#Take the remaining 20% for testing
TestData <- italymoviesnew[!(italymoviesnew$film_title%in%TrainData$film_title), ]

#Divide the train-data into sub-tables according to genre
action<-subset(TrainData, subset= TrainData$genre=="Action")
comedy<-subset(TrainData, subset= TrainData$genre=="Comedy")
drama<-subset(TrainData, subset= TrainData$genre=="Drama")
horror<-subset(TrainData, subset= TrainData$genre=="Horror")
fantasy<-subset(TrainData, subset= TrainData$genre=="Fantasy")
thriller<-subset(TrainData, subset= TrainData$genre=="Thriller")

tabels <- list(action, comedy, drama, fantasy, horror, thriller)
```

###Visualization of the data in each genre
We built a graph for each genre, that shows almost all variables of interest (each dot represent a movie):
* amount of viewers as the color of the datapoint
* length of the movie as the size of a datapoint
* the average score the movie got
* the year at which the movie was published (we will see next the impact of the release season- if thare is any)

```{r,echo=FALSE}
ACTION<- ggplot(action, aes(x=year,y=avg_vote,color=votes), xlim=c(min(action$year),max(action$year))) + geom_point(size=as.integer(action$length)) + labs(title="Action movies: average score against amount of viewers over the years")

####

COMEDY<- ggplot(comedy, aes(x=year,y=avg_vote,color=votes)) + geom_point(size=as.integer(comedy$length)) + labs(title="Comedy movies: average score against amount of viewers over the years")

####

DRAMA<- ggplot(drama, aes(x=year,y=avg_vote,color=votes)) + geom_point(size=as.integer(drama$length)) + labs(title="Drama movies: average score against amount of viewers over the years")

####

HORROR<- ggplot(horror, aes(x=year,y=avg_vote,color=votes)) + geom_point(size=as.integer(horror$length)) + labs(title="Horror movies: average score against amount of viewers over the years")

####

FANTASY<- ggplot(fantasy, aes(x=year,y=avg_vote,color=votes)) + geom_point(size=as.integer(fantasy$length)) + labs(title="Fantasy movies: average score against amount of viewers over the years")

#####

THRILLER<- ggplot(thriller, aes(x=year,y=avg_vote,color=votes)) + geom_point(size=as.integer(thriller$length)) + labs(title="Thriller movies: average score against amount of viewers over the years")
```

**The brighter and higher the point in the graph- the more popular the movie is.**


```{r, echo=FALSE}
par(mfrow=c(3,2))
ACTION
COMEDY
DRAMA
HORROR
FANTASY
THRILLER
```

From these graphs we learn that:
 1. in the last few years the amount of movies in each genre has increased according to the increasement in the datapoints density.

 2.  Action movies are successfull compare to other genres, since the spred of the points through y axis is relatively steady.
 
 3. Darma and comedy has the same trand all these years, nontheless drama is more successful (according to average score).

 4. We see that over the years the popularity of horror movies has decreased. not only from the aspect of the quality of the movie (average score shows negetive trand), but also from the aspect of amount of viewers (according to the dark color).

 5. From Fantasy graph we cannot reach the same conclusion we mentioned above.

 6. We see that over the years the popularity of thriller movies has decreased. 
 
 6. We cannot analyse the data of duration from the graphs.

##Inference
###Non-segregated genres: Find a linear relationship between variables and average movie scores
we want to check if there is a linear relationship between:
<br/> -the movie length and its average rating
<br/> -the month the movie came out and its average rating 
<br/> -the year the movie came out and its average rating

Since we could not get a sense of the relation between duration to popularity from the above graphs, we will start analysing this variable.
```{r}
summary(lm(avg_vote~duration, data = italymoviesnew)) 
```
we got a very low p-value but R^2 is not good enough (we wish to have a value closer to 1 as much as possible).

```{r}
summary(lm(avg_vote~Release_Season, data = italymoviesnew))
```
We received a linear relationship at: April, May, June and September

```{r}
summary(lm(avg_vote~year, data = italymoviesnew))
```
We accepted that there is a weak linear relationship

###Calculate the correlation coefficient squared for each genre (correlation between movie score and movie duration)
```{r}
R2 <- c(1:6)
for(j in 1:6)
{
  a <- lm(avg_vote~duration, data = tabels[[j]])
  R2[j] <- summary(a)$r.squared
}
R2
```
**we recieved no corolation between score of movies to duration of movies.**

##A linearity test between the continuous variable of movie rating and continuous variable of movie duration

```{r}
#action

action_original_model<-lm(avg_vote~duration, data=action)
#choose_model = step(original_model,.~.+Release_Season, direction = "forward")

par(mfrow=c(1,3))
#plot the SLR
plot(action$avg_vote ~ action$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = action");abline(action_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(action_original_model));qqline(resid(action_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(action_original_model), resid(action_original_model)); abline(h = 0,col=5, lwd=2)

#boxcox transformation
boxcox(action_original_model, plotit = T)
#lamda equal~1 - not good, we do transformation on x

par(mfrow=c(1,3))
action_log_model<-lm(avg_vote~log(duration), data=action)

#plot the log model on the predictor
plot(action$avg_vote ~ log(action$duration), xlab = "log(duration (min))", ylab = "avg_votes", main = "genre = action")
#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(action_log_model));qqline(resid(action_log_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(action_log_model), resid(action_log_model)); abline(h = 0, col=5, lwd=2)


summary(action_original_model)$r.squared ;summary(action_log_model)$r.squared 

```

**conclusion - there is no corolation between the score rate the movie got to the duration of it, for action genre **


```{r}
#comedy

comedy_original_model<-lm(avg_vote~duration, data=comedy)
#choose_model = step(original_model,.~.+Release_Season, direction = "forward")

par(mfrow=c(3,3))
#plot the SLR
plot(comedy$avg_vote ~ comedy$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = comedy");abline(comedy_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(comedy_original_model));qqline(resid(comedy_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(comedy_original_model), resid(comedy_original_model)); abline(h = 0, col=5, lwd=2)

#boxcox transformation
boxcox(comedy_original_model, plotit = T)
#lamda equal ~1.5 - we do boxcox transformation
ycomedy= ((comedy$avg_vote^1.5)-1)/1.5
comedy_box_model<-lm(ycomedy~comedy$duration)
#Shapiro-Wilk test. The null hypothesis for this test is that the data are normally distributed.
shapiro.test(resid(comedy_box_model))# reject null hypothesis. this transformation is not good. we try log(x) transformation.

par(mfrow=c(1,3))
comedy_log_model<-lm(avg_vote~log(duration), data=comedy)

#plot the log model on the predictor
plot(comedy$avg_vote ~ log(comedy$duration), xlab = "log(duration (min))", ylab = "avg_votes", main = "genre = comedy");abline(comedy_original_model, col = 'blue')
#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(comedy_log_model));qqline(resid(comedy_log_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(comedy_log_model), resid(comedy_log_model)); abline(h = 0, col=5, lwd=2)


summary(comedy_original_model)$r.squared  ;summary(comedy_box_model)$r.squared ;summary(comedy_log_model)$r.squared
```
**conclusion - there is no corolation between the score rate the movie got to the duration of it, for comedy genre**

```{r}
#drama

drama_original_model<-lm(avg_vote~duration, data=drama)
#choose_model = step(original_model,.~.+Release_Season, direction = "forward")

par(mfrow=c(1,3))
#plot the SLR
plot(drama$avg_vote ~ drama$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = drama");abline(drama_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(drama_original_model));qqline(resid(drama_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(drama_original_model), resid(drama_original_model)); abline(h = 0, col=5, lwd=2)

#boxcox transformation
boxcox(drama_original_model, plotit = T)
#lamda equal ~2 - not good, we do boxcox transformation
ydrama= ((drama$avg_vote^2)-1)/2
drama_box_model<-lm(ydrama~drama$duration)
#Shapiro-Wilk test
shapiro.test(resid(drama_box_model))# we get pvalue<0.05, thus we reject null hypothesis of normal distribution. 
#we priviously saw that log(x) model is not relevant regarding lambda (!=0), so thare is no point to try log(x).


summary(drama_original_model)$r.squared ;summary(drama_box_model)$r.squared
```
**we again got lower R^2 when we try boxcox transformation, compare to state of no transformation.<\br> conclusion - there is no corolation between the score rate the movie got to the duration of it, for drama genre**

```{r}
#horror

horror_original_model<-lm(avg_vote~duration, data=horror)
#choose_model = step(original_model,.~.+Release_Season, direction = "forward")

par(mfrow=c(1,3))
#plot SLR model
plot(horror$avg_vote ~ horror$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = horror");abline(horror_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(horror_original_model));qqline(resid(horror_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(horror_original_model), resid(horror_original_model)); abline(h = 0, col=5, lwd=2)

#boxcox transformation
boxcox(horror_original_model, plotit = T)
#lamda equal ~1.3 - not good, we do boxcox transformation
yhorror=((horror$avg_vote^1.3)-1)/1.3
horrorbox_model<-lm(yhorror~horror$duration)
#Shapiro-Wilk test
shapiro.test(resid(horrorbox_model))# we get pvalue>0.05, thus we except null hypothesis of normal distribution.


summary(horror_original_model)$r.squared ;summary(horrorbox_model)$r.squared 
```
**even tough we got pvalue>0.05 for shapiro-wilk test, when we take in acount R^2 values, it look like boxcox transformation is not useful.<\br> conclusion - there is no corolation between the score rate the movie got to the duration of it, for horror genre **

```{r}
#fantasy

fantasy_original_model<-lm(avg_vote~duration, data=fantasy)

par(mfrow=c(1,3))
#plot the SLR
plot(fantasy$avg_vote ~ fantasy$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = fantasy");abline(fantasy_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(fantasy_original_model));qqline(resid(fantasy_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(fantasy_original_model), resid(fantasy_original_model)); abline(h = 0, col=5, lwd=2)

#boxcox transformation
boxcox(fantasy_original_model, plotit = T)
#lamda equal ~1.2, we try boxcox transformation
yfantasy=((fantasy$avg_vote^1.2)-1)/1.2
fantasy_box_model<-lm(yfantasy~fantasy$duration)
#Shapiro-Wilk test
shapiro.test(resid(fantasy_box_model))#we get pvalue>0.05, thus we except null hypothesis of normal distribution.


summary(fantasy_original_model)$r.squared ;summary(fantasy_box_model)$r.squared 
```
**conclusion - there is no corolation between the score rate the movie got to the duration of it, for fantasy genre**

```{r}
#thriller

thriller_original_model<-lm(avg_vote~duration, data=thriller)

par(mfrow=c(1,3))
#plot the SLR
plot(thriller$avg_vote ~ thriller$duration, xlab = "duration (min)", ylab = "avg_votes", main = "genre = thriller");abline(thriller_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(thriller_original_model));qqline(resid(thriller_original_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(thriller_original_model), resid(thriller_original_model)); abline(h = 0, col=5, lwd=2)

#boxcox transformation
boxcox(thriller_original_model, plotit = T)
#lamda equal ~1 - not good, we do transformation on x

par(mfrow=c(1,3))
thriller_log_model<-lm(avg_vote~log(duration), data=thriller)

#plot the log model on the predictor
plot(thriller$avg_vote ~ log(thriller$duration), xlab = "log(duration (min))", ylab = "avg_votes", main = "genre = thriller");abline(thriller_original_model, col = 'blue')

#linerity tests
#normal distribution of the residuals tests
qqnorm(resid(thriller_log_model));qqline(resid(thriller_log_model), col=7, lwd=2)
#equal variability of the residuals tests
plot(fitted(thriller_log_model), resid(thriller_log_model)); abline(h = 0, col=5, lwd=2)


summary(thriller_original_model)$r.squared ;summary(thriller_log_model)$r.squared 
```
**conclusion - there is no corolation between the score the movie got to the duration of it, for thriller genre**


##Checking which season you should release for each genre
```{r}
par(mfrow=c(2,2))

ViewSpring <- subset(TrainData, subset = TrainData$Release_Season=="Spring")
Action <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Action"))
Comedy <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Comedy"))
Drama <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Drama"))
Fantasy <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Fantasy"))
Horror <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Horror"))
Thriller <- nrow(subset(ViewSpring, subset = ViewSpring$genre=="Thriller"))
View_at_spring <- c(Action, Comedy, Drama, Fantasy, Horror, Thriller)
plot(View_at_spring, main = "Spring", ylab = "Number of views" , xlab = "Genre
     1-Action, 2-Comedy, 3-Drama, 4-Fantasy, 5-Horror, 6-Thriller")

ViewAutumn <- subset(TrainData, subset = TrainData$Release_Season=="Autumn")
Action <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Action"))
Comedy <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Comedy"))
Drama <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Drama"))
Fantasy <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Fantasy"))
Horror <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Horror"))
Thriller <- nrow(subset(ViewAutumn, subset = ViewAutumn$genre=="Thriller"))
View_at_Autumn <- c(Action, Comedy, Drama, Fantasy, Horror, Thriller)
plot(View_at_Autumn, main = "Autumn", ylab = "Number of views" , xlab = "Genre
     1-Action, 2-Comedy, 3-Drama, 4-Fantasy, 5-Horror, 6-Thriller")

ViewWinter <- subset(TrainData, subset = TrainData$Release_Season=="Winter")
Action <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Action"))
Comedy <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Comedy"))
Drama <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Drama"))
Fantasy <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Fantasy"))
Horror <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Horror"))
Thriller <- nrow(subset(ViewWinter, subset = ViewWinter$genre=="Thriller"))
View_at_Winter <- c(Action, Comedy, Drama, Fantasy, Horror, Thriller)
plot(View_at_Winter, main = "Winter", ylab = "Number of views" , xlab = "Genre
     1-Action, 2-Comedy, 3-Drama, 4-Fantasy, 5-Horror, 6-Thriller")

ViewSummer <- subset(TrainData, subset = TrainData$Release_Season=="Summer")
Action <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Action"))
Comedy <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Comedy"))
Drama <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Drama"))
Fantasy <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Fantasy"))
Horror <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Horror"))
Thriller <- nrow(subset(ViewSummer, subset = ViewSummer$genre=="Thriller"))
View_at_Summer <- c(Action, Comedy, Drama, Fantasy, Horror, Thriller)
plot(View_at_Summer, main = "Summer", ylab = "Number of views" , xlab = "Genre
     1-Action, 2-Comedy, 3-Drama, 4-Fantasy, 5-Horror, 6-Thriller")
```
 From this graphs we can conclude:
 1. If your movie is an action genre -> you should release it in the Winter
 2. If your movie is a comedy genre -> you should release it in the Autumn (during this Season it is even more watched than Drama movies).
 3. If your movie is a drama genre -> you should release it in the Autumn. Although it is not the most viewed genre at this season, it is still more watched than the other seasons.
 4. If your movie is a fantasy genre -> you should release it in the Autumn.
 5. If your movie is a horror genre -> you should release it in the Autumn. The number of views is pretty much the same in Autumn, winter and spring, but just not in summer where the number of views drops significantly.
 6. If your movie is a Thriller genre -> you should release it in the Autumn.


##Conclusions
After clearing the data and selecting only the parts that gave us reliable information. We added information we collected from the Internet and We have shown a number of considerations that a film producer can use and thus improve the chances of his film being successful and getting a high rating.

Of course, there is still a lot of work to be done to make the movie producer more reliable as to whether or not the movie will succeed.
We did our best to filter, collect and display the results we got.

Hope you enjoyed it :)

*We know there are things that we can improved and done better, but the part of the web-scraping took us a long time to learn how to do it right and it wasn't easy at all. We hope we did it good enough.

Thank's
